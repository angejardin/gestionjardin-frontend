import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MeteoService } from '../common/service/meteo.service';
import { formatDate } from '@angular/common';
import { UsersService } from 'src/app/common/service/users.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  meteoprobawind70: string;
  meteoprobawind100: string;
  meteoprobagel: string;
  displaygel: boolean=false;
  displayvent: boolean=false;

  constructor(private userService: UsersService,
    private meteoservice: MeteoService,
    public router: Router) { }

  ngOnInit(): void {
    let date = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');

    //On récupère d'abord le code insee de l'utilisateur puis on fait les requêtes météo en conséquence
    this.userService.getUserProfile().subscribe(res => {
      let insee = res["codeinsee"];
  
      this.meteoservice.getVent(insee, String(date)).subscribe(data => {
        this.meteoprobawind70 = data["probawind70"];
        this.meteoprobawind100 = data["probawind100"];
        if(((Number(this.meteoprobawind70))>0) || ((Number(this.meteoprobawind100))>0)){
          this.displayvent=true;
        }
        this.meteoservice.getGel(insee, String(date)).subscribe(data => {
          this.meteoprobagel = data["probafrost"];
          if((Number(this.meteoprobagel))>0){
            this.displaygel=true;
          }
          
          let messagealert = "";
          if (localStorage.getItem('justconnect') == "true"){
            if (this.displaygel && this.displayvent){            
              messagealert += "Attention au risque de gel et de vent fort dans les prochains jours.";
            }
            else if (this.displaygel || this.displayvent){            
              if (this.displaygel){
                messagealert += "Attention au risque de gel dans les prochains jours.";
              }
              if (this.displayvent){
                messagealert += "Attention au risque de vent fort dans les prochains jours.";
              }
            }
            if (messagealert != ""){
              window.alert(messagealert);
            }
            localStorage.setItem('justconnect', "false")
          }
          
          
        });
        
      });
    });

  }


  jardinLink(){
    this.router.navigate(['jardin']);
  }

  meteoLink(){
    this.router.navigate(['meteo']);
  }

  profileLink(){
    this.router.navigate(['profile']);
  }

  aprevoirLink(){
    this.router.navigate(['aprevoir']);
  }
}
