import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/common/data/user';
import { UsersService } from 'src/app/common/service/users.service';
import { VilleService } from 'src/app/common/service/ville.service';
import { passwordValidator } from 'src/app/password-validator';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {

  user: User = new User();

  afficherChgtMDP: boolean = false;
  afficherChgtInfos: boolean = false;

  mdpForm: FormGroup;
  infosForm: FormGroup;

  jsonvilles: String[] = [];
  selectedville: String;
  codeinseeselected: String;

  constructor(
    public userService: UsersService,
    public villeService: VilleService,
    public router: Router
  ) {}

  ngOnInit() {

    this.userService.getUserProfile().subscribe(data => {
      this.user.username = data["username"];
      this.user.firstname = data["firstname"];
      this.user.lastname = data["lastname"];
      this.user.email = data["email"];
      this.user.birthday = data["birthday"];
      this.user.codepostal = data["codepostal"];
      this.user.ville = data["ville"];
      this.selectedville = data["ville"];
    });


    this.infosForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(6)]),
      firstname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(6)]),
      birthday: new FormControl('', [Validators.required]),
      codepostal: new FormControl('', [Validators.required, Validators.minLength(5)]),
      ville: new FormControl('', [Validators.required])
    })



    this.mdpForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password2: new FormControl('', [Validators.required, Validators.minLength(6)])
    },
      { validators: passwordValidator })

  }


  /////// Fct pour supprimer le compte //////
  SupprimerCompte() {
    let resultat = window.confirm("Voulez-vous vraiment supprimer votre compte ?");
    if (resultat == true) {
      this.userService.deleteUserProfile().subscribe(data => {
        if (data == true) {
          window.alert("Votre compte a bien été supprimé.");
          localStorage.removeItem('access_token');
          localStorage.removeItem('username');
          localStorage.removeItem('id');
          this.router.navigate(['register']);
        }
        else {
          window.alert("Il y a eu un problème lors de la suppression de votre compte.");
        }
      });
    }
  }


  /////// Fct pour afficher le formulaire de modification du mot de passe //////
  AffichageModifierMDP() {
    this.afficherChgtMDP = true;
  }

  /////// Fct pour annuler la modification du mot de passe //////
  AnnulerModifierMDP() {
    this.afficherChgtMDP = false;
  }

  /////// Fct pour modifier le mot de passe //////
  ModifierMDP() {
    this.userService.modifUserPassword(this.mdpForm.get('password').value)
    this.afficherChgtMDP = false;
  }


  /////// Fct pour afficher le formulaire de modification des informations //////
  AffichageModifierInfos() {
    this.infosForm.get('username').setValue(this.user.username)
    this.infosForm.get('firstname').setValue(this.user.firstname)
    this.infosForm.get('lastname').setValue(this.user.lastname)
    this.infosForm.get('email').setValue(this.user.email)
    this.infosForm.get('birthday').setValue(this.user.birthday)
    this.infosForm.get('codepostal').setValue(this.user.codepostal)
    this.infosForm.get('ville').setValue(this.user.ville)
    this.jsonvilles.push(this.user.ville)
    this.afficherChgtInfos = true;
  }
  

  /////// Fct pour annuler la modification des informations //////
  AnnulerModifierInfos() {
    this.afficherChgtInfos = false;
  }


  /////// Fct pour modifier les informations //////
  ModifierInfos() {
    this.user.username = this.infosForm.get('username').value
    this.user.firstname = this.infosForm.get('firstname').value
    this.user.lastname = this.infosForm.get('lastname').value
    this.user.email = this.infosForm.get('email').value
    if (this.user.birthday != this.infosForm.get('birthday').value){
      this.user.birthday = this.infosForm.get('birthday').value.toLocaleDateString()
    }
    else{
      this.user.birthday = this.infosForm.get('birthday').value
    }
    
    this.user.codepostal = this.infosForm.get('codepostal').value
    this.user.ville = this.infosForm.get('ville').value

    this.villeService.getInsee(this.user.ville).subscribe(res => {
      //Parcours de la liste pour retrouver le bon nom de ville
      for (let i = 0; i < res.length; i++) {
        if (res[i].nom == this.user.ville) {
          this.user.codeinsee = res[i].code
        }
      }
      this.userService.modifUserProfile(this.user);
    }); 
    this.afficherChgtInfos = false;   
  }



  // Quand le code postal change, on met à jour la liste des villes possibles
  codePostChange(e) {
    if (e.length > 4) {
      this.jsonvilles = []
      this.villeService.getVille(e).subscribe(res => {
        res.forEach(element => {
          this.jsonvilles.push(element['nom'])
        });
      })
    }
  }

  

  // Quand le code postal change, on met à jour la liste des villes possibles
  villeChange() {
    this.selectedville = this.infosForm.value["ville"]
    this.villeService.getInsee(this.selectedville).subscribe(res => {
      //Parcours de la liste pour retrouver le bon nom de ville
      for (let i = 0; i < res.length; i++) {
        if (res[i].nom == this.selectedville) {
          this.codeinseeselected = res[i].code
        }
      }
    });
  }

}
