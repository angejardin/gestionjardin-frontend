import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/common/data/user';
import { AuthService } from 'src/app/common/service/auth.service';
import { VilleService } from 'src/app/common/service/ville.service';
import { passwordValidator } from 'src/app/password-validator';


@Component({
  selector: 'app-userregister',
  templateUrl: './userregister.component.html',
  styleUrls: ['./userregister.component.scss']
})
export class UserregisterComponent implements OnInit {

  signupForm: FormGroup;

  jsonvilles: String[] = [];
  selectedville: String;
  codeinseeselected: String;

  user: User;


  constructor(
    public authService: AuthService,
    public villeService: VilleService,
    public router: Router
  ) {
    this.signupForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password2: new FormControl('', [Validators.required, Validators.minLength(6)]),
      firstname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(6)]),
      birthday: new FormControl('', [Validators.required]),
      codepostal: new FormControl('', [Validators.required, Validators.minLength(5)]),
      ville: new FormControl('', [Validators.required]),
      mentions: new FormControl('', [Validators.required])
    },
      { validators: passwordValidator })
  }

  ngOnInit() {
  }

  registerUser() {
    this.user = new User()
    this.user.username = this.signupForm.get('username').value
    this.user.password = this.signupForm.get('password').value
    this.user.firstname = this.signupForm.get('firstname').value
    this.user.lastname = this.signupForm.get('lastname').value
    this.user.email = this.signupForm.get('email').value
    this.user.birthday = this.signupForm.get('birthday').value.toLocaleDateString()
    this.user.codepostal = this.signupForm.get('codepostal').value
    this.user.ville = this.selectedville
    this.user.codeinsee = this.codeinseeselected

    this.authService.signUp(this.user)
  }

  // Quand le code postal change, on met à jour la liste des villes possibles
  codePostChange(e) {
    if (e.length > 4) {
      this.jsonvilles = []
      this.villeService.getVille(e).subscribe(res => {
        res.forEach(element => {
          this.jsonvilles.push(element['nom'])
        });
      })
    }
  }

  // Récupération du code insee
  villeChange() {
    this.selectedville = this.signupForm.value["ville"]
    this.villeService.getInsee(this.selectedville).subscribe(res => {
      //Parcours de la liste pour retrouver le bon nom de ville
      for (let i = 0; i < res.length; i++) {
        if (res[i].nom == this.selectedville) {
          this.codeinseeselected = res[i].code
        }
      }
    });
  }

  
  mentionOK: boolean = false;  
  changeCheck(event){
    this.mentionOK = event.target.checked;
  }


}
