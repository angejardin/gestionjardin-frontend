import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserconnexionComponent } from './userconnexion.component';

describe('UserconnexionComponent', () => {
  let component: UserconnexionComponent;
  let fixture: ComponentFixture<UserconnexionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserconnexionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserconnexionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
