import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/common/data/user';
import { AuthService } from 'src/app/common/service/auth.service';
import { UsersService } from 'src/app/common/service/users.service';

@Component({
  selector: 'app-userconnexion',
  templateUrl: './userconnexion.component.html',
  styleUrls: ['./userconnexion.component.scss']
})
export class UserconnexionComponent implements OnInit {

  signinForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) {
    this.signinForm = this.fb.group({
      username: [''],
      password: ['']
    })
  }

  ngOnInit() { }

  loginUser() {
    this.authService.signIn(this.signinForm.value)
  }

}
