import { ValidationErrors, FormGroup, ValidatorFn } from "@angular/forms";

export const passwordValidator: ValidatorFn = (
    control: FormGroup
): ValidationErrors | null => {
    const password = control.get("password").value;
    const confirmPassword = control.get("password2").value;
    return password && confirmPassword && password === confirmPassword
        ? null
        : { passwordsNotEqual: true };
};