export class User{
    public _id:String;
    public username:String;
    public password:String;
    public firstname:String;
    public lastname:String;
    public email:String;
    public birthday:String;
    public codepostal:String;
    public ville:String;
    public codeinsee:String;
}