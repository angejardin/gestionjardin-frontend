export class Legumes{
    constructor(public nom:string="?",
                public besoinEauCroissanceNord:string="?",
                public besoinEauProductionNord:string="?",
                public besoinEauCroissanceSud:string="?",
                public besoinEauProductionSud:string="?",
                public semisSousAbris:Number[]=[],
                public semisEnPlace:Number[]=[],
                public Plantation:Number[]=[],
                public Recolte:Number[]=[],
                ){}

}