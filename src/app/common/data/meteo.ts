export class Meteo{
    constructor(public insee:string="?",
                public cp:string="?",
                public latitude:string="?",
                public longitude:string="?",
                public day:string="?",
                public datetime:string="?",
                public wind10m:string="?",
                public gust10m:string="?",
                public dirwind10m:string="?",
                public rr10:string="?",
                public rr1:string="?",
                public probarain:string="?",
                public weather:string="?",
                public tmin:string="?",
                public tmax:string="?",
                public sun_hours:string="?",
                public etp:string="?",
                public probafrost:string="?",
                public probafog:string="?",
                public probawind70:string="?",
                public probawind100:string="?",
                public gustx:string="?"){}
}