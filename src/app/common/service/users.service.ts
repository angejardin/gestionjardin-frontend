import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../data/user';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Jardin } from '../data/jardin';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUrl: string = environment.backendSpringUsers.baseURL;
  _apiBaseUrl = `${this.baseUrl}`;

  constructor(private _http: HttpClient) { } 

  // Obtenir le profil de l'utilisateur
  getUserProfile(): Observable<any> {
    let api = this._apiBaseUrl + "/api/data/profile/" + localStorage.getItem('username');
    return this._http.get(api, {responseType: 'json'})
  }

  // Modifier les informations de l'utilisateur
  modifUserProfile(user: User) {
    let api = this._apiBaseUrl + "/api/data/profile/" + localStorage.getItem('id');
    return this._http.put(api, user).subscribe(
    data => console.log('put user ok'),
    error => window.alert(error.error.message))
  }

  // Modifier les informations de l'utilisateur
  modifUserPassword(password: String) {
    let api = this._apiBaseUrl + "/api/data/profilePassword/" + localStorage.getItem('id');
    return this._http.put(api, password).subscribe(
    data => console.log('put mdp ok'),
    error => window.alert(error.error.message))
  }

  // Supprimer le profil de l'utilisateur
  deleteUserProfile(): Observable<any> {
    let api = this._apiBaseUrl + "/api/data/profile/" + localStorage.getItem('id');
    return this._http.delete(api, {responseType: 'json'})
  }

  // Obtenir le contenu du jardin de l'utilisateur
  getUserJardin(): Observable<Jardin> {
    let api = this._apiBaseUrl + "/api/data/jardin/" + localStorage.getItem('id');
    return this._http.get<Jardin>(api)
  }

  // Sauvegarder le contenu du jardin de l'utilisateur
  postUserJardin(jardin: Jardin) {
    let api = this._apiBaseUrl + "/api/data/jardin";
    return this._http.post(api, jardin)
    .subscribe(data => console.log('post jardin ok'),
    error => window.alert(error.error.message))
  }

  // Modifier le contenu du jardin de l'utilisateur
  putUserJardin(jardin: Jardin) {
    let api = this._apiBaseUrl + "/api/data/modifjardin/" + localStorage.getItem('id');
    return this._http.put(api, jardin)
    .subscribe(data => console.log('put jardin ok'),
    error => window.alert(error.error.message))
  }

  // Données admin
  getAdminContent(): Observable<any> {
    let api = this._apiBaseUrl + "/api/data/admin";
    return this._http.get(api, {responseType: 'text'})
  }

}
