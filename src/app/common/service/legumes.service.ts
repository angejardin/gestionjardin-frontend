import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Legumes } from '../data/legumes';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LegumesService {

  baseUrl: string = environment.backendSpringLegumes.baseURL + '/legumes';

  private _headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private _http: HttpClient) { }

  getAllName(): Observable<Array<String>> { 
    return this._http.get<Array<String>>(`${this.baseUrl}` + '/listeLegumes');
  }

  getInfosLegume(nom: String): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/infosLegume/' + nom.split(" ").join("%20"));
  }

}
