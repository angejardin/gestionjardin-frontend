import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { User } from '../data/user';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl: string = environment.backendSpringUsers.baseURL;

  _apiBaseUrl = `${this.baseUrl}`;
  

  currentUser = {};

  constructor(
    private http: HttpClient,
    public router: Router
  ) {}

  // Sign-up
  signUp(user: User) {
    let api = this._apiBaseUrl + "/api/auth/signup";
    return this.http.post(api, user)
    .subscribe(data => {
      window.alert("Utilisateur enregistré, cliquer sur OK pour aller sur la page de connexion.")
      this.router.navigate(['connexion']);
    },
    error => window.alert(error.error.message))
  }


  // Sign-in
  signIn(user: User) {
    let api = this._apiBaseUrl + "/api/auth/signin";
    return this.http.post<any>(api, user)
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res["accessToken"])
        localStorage.setItem('username', res["username"])
        localStorage.setItem('id', res["id"])
        localStorage.setItem('justconnect', "true")
        this.router.navigate(['home']);
        
      },
      error => window.alert(error.error.message))
  }
  

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return (authToken !== null) ? true : false;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    localStorage.removeItem('username');
    localStorage.removeItem('id');
    if (removeToken == null) {
      this.router.navigate(['connexion']);
    }
  }

}
