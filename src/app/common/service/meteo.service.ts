import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Meteo } from '../data/meteo';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {

  baseUrl: string = environment.backendNodeJS.baseURL;

  private _headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private _http: HttpClient) { }

  getAll(): Observable<Array<Meteo>> {
    return this._http.get<Array<Meteo>>(`${this.baseUrl}` + '/apimeteo/prev');
  }

  getPrec7j(codeinsee: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/precipitation7j/' + codeinsee);
  }

  getPrecDay(codeinsee: string, date: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/precipitation/' + codeinsee + '/' + date);
  }

  getVent(codeinsee: string, date: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/vent/' + codeinsee + '/' + date);
  }

  getGel(codeinsee: string, date: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/gel/' + codeinsee + '/' + date);
  }

  getSoleil(codeinsee: string, date: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/soleil/' + codeinsee + '/' + date);
  }

  getTemp(codeinsee: string, date: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}` + '/apimeteo/temperature/' + codeinsee + '/' + date);
  }

}
