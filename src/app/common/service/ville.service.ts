import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  baseUrl: string = environment.backendVille.baseURL;
  _apiBaseUrl = `${this.baseUrl}`;

  constructor(private _http: HttpClient) { }


  // Récupération de la liste de ville à partir du code postal
  getVille(codepostal): Observable<any> {
    let api = this._apiBaseUrl + "/communes?codePostal=" + codepostal;
    return this._http.get(api, {responseType: 'json'})
  }

  // Récupération du code INSEE à partir de la ville
  getInsee(ville): Observable<any> {
    let api = this._apiBaseUrl + "/communes?nom=" + ville;
    return this._http.get(api, {responseType: 'json'})
  }
}
