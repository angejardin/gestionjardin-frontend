import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AprevoirComponent } from './aprevoir.component';

describe('AprevoirComponent', () => {
  let component: AprevoirComponent;
  let fixture: ComponentFixture<AprevoirComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AprevoirComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AprevoirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
