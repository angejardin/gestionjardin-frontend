import { Component, OnInit } from '@angular/core';
import { Jardin } from '../common/data/jardin';
import { LegumesService } from '../common/service/legumes.service';
import { UsersService } from '../common/service/users.service';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-aprevoir',
  templateUrl: './aprevoir.component.html',
  styleUrls: ['./aprevoir.component.scss']
})
export class AprevoirComponent implements OnInit {

  /////// deb : Variables globales //////
  form: FormGroup; //form pour gérer les checbox de la liste de légumes
  semisSAForm: FormGroup; //form pour gérer les checbox de la liste de légumes à semer sous abris
  semisSPForm: FormGroup; //form pour gérer les checbox de la liste de légumes à semer sur place
  plantForm: FormGroup; //form pour gérer les checbox de la liste de légumes à planter
  arrosageForm: FormGroup; //form pour gérer les checbox de la liste de légumes à planter
  legumes: Array<{ name: String, checked: boolean }> = []; //Liste des légumes possibles, avec leur nom et la valeur de checked correspondant à la sélection des légumes par l'utilisateur
  legumesduMoisaSemerSA: Array<{ name: String, checked: boolean, mois:string[] }> = []; //Liste des légumes que l'on peut semer sous abris dans le mois
  legumesduMoisaSemerSP: Array<{ name: String, checked: boolean, mois:string[] }> = []; //Liste des légumes que l'on peut semer sur place dans le mois
  legumesduMoisaPlanter: Array<{ name: String, checked: boolean, mois:string[] }> = []; //Liste des légumes que l'on peut planter dans le mois
  jardinuser: Jardin = new Jardin([], [], [], [], "") //jardin de l'utilisateur
  jardinvide: boolean = true; // Permet de gérer l'affichage de la liste des légumes en fonction de si la liste est vide ou non
  messagejardinvide: String = "Vous n'avez pas encore choisi de légumes à mettre dans votre jardin"; //gestion du message a afficher si le jardin est vide
  fairechoix: boolean = false; // gestion de l'affichage de la liste de checkbox legumes souhaites
  semischoix: boolean = false; // gestion de l'affichage de la liste de checkbox legumes semés
  plantchoix: boolean = false; // gestion de l'affichage de la liste de checkbox legumes plantés
  semisSAvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des semis sous abris a prevoir
  semisSPvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des semis sur place a prevoir
  messagepasdesemis: String = "Vous n'avez pas de semis à prevoir"; //gestion du message a afficher s'il n'y a pas de semis a prevoir
  plantvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des plantations a prevoir
  messagepasdeplant: String = "Vous n'avez pas de plantation à prevoir"; //gestion du message a afficher s'il n'y a pas de plantation a prevoir
  arrosagevide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des plantations a prevoir
  messagepasdarrosage: String = "Vous n'avez rien a arroser"; //gestion du message a afficher s'il n'y a pas de plantation a prevoir
  months: string[] = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
  /////// fin : Variables globales //////



  /////// deb : Constructeur //////
  constructor(private legumeservice: LegumesService,
    private usersservice: UsersService) { }
  /////// fin : Constructeur //////



  /////// deb : Fonction lancée après initialisation du composant //////
  ngOnInit(): void {
    // création d'un groupe "légumes" pour l'afficahge de la liste de légumes a choisir avec des checkbox
    this.form = new FormGroup({ legumes: new FormArray([]) });

    //Récupérer la liste des légumes de l'utilisateur    
    this.usersservice.getUserJardin().subscribe(
      data => {
        //Si rien :
        // - créer un jardin vide avec le userid de l'utilisateur
        // - afficher : Vous n'avez pas encore choisi de légumes à mettre dans votre jardin
        if (data == null) {
          this.jardinuser = new Jardin([], [], [], [], localStorage.getItem('id'))
          this.jardinvide = true;
        }
        //Sinon afficher la liste des légumes choisis
        else {
          this.jardinuser = data;
          this.jardinvide = false;
        }

        //Prise en compte des legumes voulus par l'utilisateur en récupérant la liste dans la BDD de l'utilisateur
        //Récupération de la liste totale des légumes
        this.legumeservice.getAllName().subscribe(data => {
          //On assigne la valeur de checked en fonction de l'appartenance du légume dans la liste des légumes voulus de l'utilisateur
          data.sort().forEach(element => this.legumes.push({ name: element, checked: this.jardinuser.legumesvoulus.includes(element) }))
          //Création de la form
          this.form = new FormGroup({
            legumes: new FormArray([]),
          });
          this.semisSAForm = new FormGroup({
            legumessemissa: new FormArray([]),
          });
          this.semisSPForm = new FormGroup({
            legumessemissp: new FormArray([]),
          });
          this.plantForm = new FormGroup({
            legumesplant: new FormArray([]),
          });
          //Création de la liste des checkbox
          this.addCheckboxes();

          this.ChangementLegumesVoulus();


        });

      }
    )

  }
  /////// fin : Fonction lancée après initialisation du composant //////



  /////// deb : Fonction pour créer la form contenant la liste de checkbox //////
  private addCheckboxes() {
    const formArray = this.form.get('legumes') as FormArray;
    //Pour chaque légumes de la base de données, on crée une checkbox
    this.legumes.forEach(legume => {
      formArray.push(new FormGroup({
        //Le nom est celui du légume
        name: new FormControl(legume.name),
        //La case est préselectionnées en fonction de la valeur de checked
        checked: new FormControl(legume.checked)
      }))
    })
  }
  /////// fin : Fonction pour créer la form contenant la liste de checkbox //////



  /////// deb : Fct pour afficher la liste de checkbox //////
  ListeVisible() {
    this.fairechoix = true;
  }
  /////// fin : Fct pour afficher la liste de checkbox //////



  /////// deb : Fct pour enregistrer ou modifier les choix de légumes de l'utilisateur //////
  ChoixFait() {
    // Récupération des légumes choisis par l'utilisateur
    const tabfiltree = this.form.value.legumes.filter(l => l.checked)
    //Réinitialisation de la liste des légumes voulus par l'utilisateur
    this.jardinuser.legumesvoulus = []
    //Ajout des légumes sélectionnés
    tabfiltree.forEach(element => this.jardinuser.legumesvoulus.push(element.name));

    //Si le jardin était précédemment vide, alors on post un nouveau jardin
    if (this.jardinvide) {
      this.usersservice.postUserJardin(this.jardinuser)
    }
    //sinon on actualise les valeurs du jardin déjà dans la BDD
    else {
      this.usersservice.putUserJardin(this.jardinuser)
    }

    this.ChangementLegumesVoulus()

    //Mise a jour de la valeur qui indique si le jardin est vide
    this.jardinvide = false;
    //Mise a jour de la valeur qui indique si le choix est validé
    this.fairechoix = false;
  }
  /////// deb : Fct pour enregistrer ou modifier les choix de légumes de l'utilisateur //////


  /////// deb : Fct pour actualiser la liste des légumes à semer et planter ce mois //////
  ChangementLegumesVoulus() {

    //Parcours de la liste des legumes souhaité pour selectionner les légumes a semer ou planter ce mois
    let date = new Date()

    this.legumesduMoisaSemerSA = []
    this.legumesduMoisaSemerSP = []
    this.legumesduMoisaPlanter = []

    this.semisSAvide = true
    this.semisSPvide = true
    this.plantvide = true

    this.jardinuser.legumesvoulus.forEach(element => {
 
      this.legumeservice.getInfosLegume(element).subscribe(res => {
          //Récupération des légumes que l'on peut semer sous abris pendant le mois en cours
          if (res["semisSousAbris"] != 0) {
            let tbmois = [];
            res["semisSousAbris"].forEach(elt => {
              tbmois.push(this.months[elt-1]) //transformation du chiffre du mois en string a l'aide du tableau months
              //Si le mois en cours correspond à un des mois possible de semis sous abris alors on affiche dans la liste
              if (date.getMonth() == (elt-1)) {
                this.legumesduMoisaSemerSA.push({ name: element, checked: false,  mois: tbmois})
                this.semisSAvide = false
              }              
            });
          }

          //Récupération des légumes que l'on peut semer en place pendant le mois en cours
          if (res["semisEnPlace"] != 0) {
            let tbmois = [];
            res["semisEnPlace"].forEach(elt => {
              tbmois.push(this.months[elt-1])  //transformation du chiffre du mois en string a l'aide du tableau months
              //Si le mois en cours correspond à un des mois possible de semis sur place alors on affiche dans la liste
              if (date.getMonth() == (elt-1)) {
                this.legumesduMoisaSemerSP.push({ name: element, checked: false,  mois: tbmois})
                this.semisSPvide = false
              }              
            });
          }

          //Récupération des légumes que l'on peut planter pendant le mois en cours
          if (res["plantation"] != 0) {
            let tbmois = [];
            res["plantation"].forEach(elt => {
              tbmois.push(this.months[elt-1])  //transformation du chiffre du mois en string a l'aide du tableau months
              //Si le mois en cours correspond à un des mois possible de plantation alors on affiche dans la liste
              if (date.getMonth() == (elt-1)) {
                this.legumesduMoisaPlanter.push({ name: element, checked: false,  mois: tbmois})
                this.plantvide = false
              }              
            });
          }

        })
      
    })

  }
  /////// fin : Fct pour actualiser la liste des légumes à semer et planter ce mois //////



  /////// deb : Fct pour dire qu'on a fait des semis //////
  SemisFait() {
    this.addCheckboxesSemisSA()
    this.addCheckboxesSemisSP()
    this.semischoix = true;

  }
  /////// fin : Fct pour dire qu'on a fait des semis //////


  /////// deb : Fonction pour créer la form contenant la liste de checkbox //////
  private addCheckboxesSemisSA() {
    this.semisSAForm = new FormGroup({ legumessemissa: new FormArray([]) });

    const formArray = this.semisSAForm.get('legumessemissa') as FormArray;
    //Pour chaque légumes de la base de données, on crée une checkbox
    this.legumesduMoisaSemerSA.forEach(legume => {
      formArray.push(new FormGroup({
        //Le nom est celui du légume
        name: new FormControl(legume.name),
        //La case est préselectionnées en fonction de la valeur de checked
        checked: new FormControl(legume.checked)
      }))
    })
  }
  /////// fin : Fonction pour créer la form contenant la liste de checkbox //////

  /////// deb : Fonction pour créer la form contenant la liste de checkbox //////
  private addCheckboxesSemisSP() {
    this.semisSPForm = new FormGroup({ legumessemissp: new FormArray([]) });

    const formArray = this.semisSPForm.get('legumessemissp') as FormArray;
    //Pour chaque légumes de la base de données, on crée une checkbox
    this.legumesduMoisaSemerSP.forEach(legume => {
      formArray.push(new FormGroup({
        //Le nom est celui du légume
        name: new FormControl(legume.name),
        //La case est préselectionnées en fonction de la valeur de checked
        checked: new FormControl(legume.checked)
      }))
    })
  }
  /////// fin : Fonction pour créer la form contenant la liste de checkbox //////


  /////// deb : Fct pour enregistrer les légumes semés par l'utilisateur //////
  ChoixFaitSemis() {
    let dateauj = new Date()
    // Récupération des légumes choisis par l'utilisateur
    const tabfiltreesa = this.semisSAForm.value.legumessemissa.filter(l => l.checked)
    // Ajout des légumes sélectionnés
    tabfiltreesa.forEach(element => this.jardinuser.legumessemessa.push(element.name + " : " + dateauj.toLocaleString().substring(0,10)));

    // Récupération des légumes choisis par l'utilisateur
    const tabfiltreesp = this.semisSPForm.value.legumessemissp.filter(l => l.checked)
    // Ajout des légumes sélectionnés
    tabfiltreesp.forEach(element => this.jardinuser.legumessemessp.push(element.name + " : " + dateauj.toLocaleString().substring(0,10)));

    // On actualise les valeurs du jardin déjà dans la BDD
    this.usersservice.putUserJardin(this.jardinuser)

    this.ChangementLegumesVoulus()

    // Mise a jour de la valeur qui indique si le choix est validé
    this.semischoix = false;
  }
  /////// deb : Fct pour enregistrer ou modifier les choix de légumes de l'utilisateur //////



  /////// deb : Fct pour dire qu'on a fait des semis //////
  PlantFait() {
    this.addCheckboxesPlant()
    this.plantchoix = true;
  }
  /////// fin : Fct pour dire qu'on a fait des semis //////


  /////// deb : Fonction pour créer la form contenant la liste de checkbox //////
  private addCheckboxesPlant() {
    this.plantForm = new FormGroup({ legumesplant: new FormArray([]) });

    const formArray = this.plantForm.get('legumesplant') as FormArray;
    //Pour chaque légumes de la base de données, on crée une checkbox
    this.legumesduMoisaPlanter.forEach(legume => {
      formArray.push(new FormGroup({
        //Le nom est celui du légume
        name: new FormControl(legume.name),
        //La case est préselectionnées en fonction de la valeur de checked
        checked: new FormControl(legume.checked)
      }))
    })
  }
  /////// fin : Fonction pour créer la form contenant la liste de checkbox //////



  /////// deb : Fct pour enregistrer les légumes semés par l'utilisateur //////
  ChoixFaitPlant() {
    let dateauj = new Date()
    // Récupération des légumes choisis par l'utilisateur
    const tabfiltreesa = this.plantForm.value.legumesplant.filter(l => l.checked)
    // Ajout des légumes sélectionnés
    tabfiltreesa.forEach(element => this.jardinuser.legumesplantes.push(element.name + " : " + dateauj.toLocaleString().substring(0,10)));

    // On actualise les valeurs du jardin déjà dans la BDD
    this.usersservice.putUserJardin(this.jardinuser)

    this.ChangementLegumesVoulus()

    // Mise a jour de la valeur qui indique si le choix est validé
    this.plantchoix = false;
  }
  /////// deb : Fct pour enregistrer ou modifier les choix de légumes de l'utilisateur //////

}
