import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteodisplayComponent } from './meteodisplay.component';

describe('MeteodisplayComponent', () => {
  let component: MeteodisplayComponent;
  let fixture: ComponentFixture<MeteodisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeteodisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteodisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
