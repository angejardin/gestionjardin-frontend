import { Component, OnInit } from '@angular/core';
import { Meteo } from '../common/data/meteo';
import { MeteoService } from '../common/service/meteo.service';
import { formatDate } from '@angular/common';
import { UsersService } from 'src/app/common/service/users.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

@Component({
  selector: 'app-meteodisplay',
  templateUrl: './meteodisplay.component.html',
  styleUrls: ['./meteodisplay.component.scss']
})
export class MeteodisplayComponent implements OnInit {

  nbml: string;
  pluiejour: string;
  meteoventvitesse: string;
  meteoventrafales: string;
  meteoventdirection: string;
  meteoprobawind70: string;
  meteoprobawind100: string;
  meteoprobagel: string;
  meteotempmin: string;
  meteotempmax: string;
  meteosoleil: string;
  displaygel: boolean=false;
  displayvent: boolean=false;


  ////// deb : Gestion des données et option du graphique en barres pour le cumul de la météo
  barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
        font: {
          size: 10,
        }
      }
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [pluginDataLabels];
  barChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(70,70,70,1)',
    },
  ];

  barChartData: ChartDataSets[] = [];
////// fin : Gestion des données et option du graphique en barres pour le cumul de la météo



  constructor(
    private userService: UsersService,
    private meteoservice: MeteoService) {
      registerLocaleData(localeFr, 'fr');
     }

  ngOnInit(): void {
    
    let date = formatDate(new Date(), 'yyyy-MM-dd', 'fr');

    //On récupère d'abord le code insee de l'utilisateur puis on fait les requêtes météo en conséquence
    this.userService.getUserProfile().subscribe(res => {
      let insee = res["codeinsee"];

      this.meteoservice.getPrec7j(insee).subscribe(data => {
        var tabval = [];
        for (let i=0; i<7; i++){ 
          tabval.push(data["precipitation7j"][i][0])
          this.barChartLabels.push(data["precipitation7j"][i][1])
        }
        this.barChartData = [{ data: tabval, label: 'Cumul de pluie sur la journée (mL)'}]
              
      });
  
      this.meteoservice.getPrecDay(insee, String(date)).subscribe(data => {
        this.pluiejour = data["precipitationday"];
      });
  
      this.meteoservice.getVent(insee, String(date)).subscribe(data => {
        this.meteoventvitesse = data["wind10m"];
        this.meteoventrafales = data["gust10m"];
        this.meteoventdirection = data["dirwind10m"];
        this.meteoprobawind70 = data["probawind70"];
        this.meteoprobawind100 = data["probawind100"];
        if(((Number(this.meteoprobawind70))>0) || ((Number(this.meteoprobawind100))>0)){
          this.displayvent=true;
        }        
      });


      this.meteoservice.getGel(insee, String(date)).subscribe(data => {
        this.meteoprobagel = data["probafrost"];
        if((Number(this.meteoprobagel))>0){
          this.displaygel=true;
        }        
      });
      

      this.meteoservice.getTemp(insee, String(date)).subscribe(data => {
        this.meteotempmin = data["tmin"];
        this.meteotempmax = data["tmax"];
      });

      this.meteoservice.getSoleil(insee, String(date)).subscribe(data => {
        this.meteosoleil = data["sunHours"];
      });

      
      

    });
    
  }



}
