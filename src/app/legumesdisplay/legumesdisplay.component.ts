import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Jardin } from '../common/data/jardin';
import { LegumesService } from '../common/service/legumes.service';
import { UsersService } from '../common/service/users.service';

@Component({
  selector: 'app-legumesdisplay',
  templateUrl: './legumesdisplay.component.html',
  styleUrls: ['./legumesdisplay.component.scss']
})
export class LegumesdisplayComponent implements OnInit {

  semisSAvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des semis
  semisSPvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des semis
  messagepasdesemis: String = "Vous n'avez pas encore de semis"; //gestion du message a afficher s'il n'y a pas de semis
  messagepasdesemissa: String = "Vous n'avez pas encore de semis sous abris"; //gestion du message a afficher s'il n'y a pas de semis
  messagepasdesemissp: String = "Vous n'avez pas encore de semis sur place"; //gestion du message a afficher s'il n'y a pas de semis
  plantvide: boolean = true; // Permet de gérer l'affichage en fct de s'il y a des plantations
  messagepasdeplant: String = "Vous n'avez pas encore de plantation"; //gestion du message a afficher s'il n'y a pas de plantation
  jardinuser: Jardin = new Jardin([], [], [], [], "") //jardin de l'utilisateur



  /////// deb : Constructeur //////
  constructor(private legumeservice: LegumesService,
    private usersservice: UsersService) {}
  /////// fin : Constructeur //////


  


  /////// deb : Fonction lancée après initialisation du composant //////
  ngOnInit(): void {

    //Récupérer la liste des légumes de l'utilisateur    
    this.usersservice.getUserJardin().subscribe(
      data => {
        //Si rien, on affiche un message de jardin vide
        if (data != null) {          
          this.jardinuser = data;
          if(this.jardinuser.legumessemessa.length>0){
            this.semisSAvide = false
          }
          if(this.jardinuser.legumessemessp.length>0){
            this.semisSPvide = false
          }
          if(this.jardinuser.legumesplantes.length>0){
            this.plantvide = false
          }
        }
      }
    )
    
  }
  /////// fin : Fonction lancée après initialisation du composant //////


  

}