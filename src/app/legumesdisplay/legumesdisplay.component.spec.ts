import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegumesdisplayComponent } from './legumesdisplay.component';

describe('LegumesdisplayComponent', () => {
  let component: LegumesdisplayComponent;
  let fixture: ComponentFixture<LegumesdisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegumesdisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegumesdisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
