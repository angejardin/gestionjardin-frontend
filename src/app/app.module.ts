import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UserregisterComponent } from './user/userregister/userregister.component';
import { UserconnexionComponent } from './user/userconnexion/userconnexion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MeteodisplayComponent } from './meteodisplay/meteodisplay.component';
import { HomeComponent } from './home/home.component';
import { LegumesdisplayComponent } from './legumesdisplay/legumesdisplay.component';
import { UserprofileComponent } from './user/userprofile/userprofile.component';
import { AuthInterceptor } from './common/service/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChartsModule } from 'ng2-charts';
import { MentionsComponent } from './mentions/mentions.component';
import { AprevoirComponent } from './aprevoir/aprevoir.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    UserregisterComponent,
    UserconnexionComponent,
    MeteodisplayComponent,
    HomeComponent,
    LegumesdisplayComponent,
    UserprofileComponent,
    MentionsComponent,
    AprevoirComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ChartsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
