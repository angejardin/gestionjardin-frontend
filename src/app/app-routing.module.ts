import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeteodisplayComponent } from './meteodisplay/meteodisplay.component';
import { UserconnexionComponent } from './user/userconnexion/userconnexion.component';
import { UserregisterComponent } from './user/userregister/userregister.component';
import { HomeComponent } from './home/home.component';
import { LegumesdisplayComponent } from './legumesdisplay/legumesdisplay.component';
import { AuthGuard } from './common/auth.guard';
import { UserprofileComponent } from './user/userprofile/userprofile.component';
import { MentionsComponent } from './mentions/mentions.component';
import { AprevoirComponent } from './aprevoir/aprevoir.component';

const routes: Routes = [
  //{ path: '', component: HomeComponent},
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'register', component: UserregisterComponent },
  { path: 'connexion', component: UserconnexionComponent },
  { path: 'meteo', component: MeteodisplayComponent, canActivate: [AuthGuard] },
  { path: 'jardin', component: LegumesdisplayComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: UserprofileComponent, canActivate: [AuthGuard] },
  { path: 'mentions', component: MentionsComponent },
  { path: 'aprevoir', component: AprevoirComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
