// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backendNodeJS: {
    baseURL: "https://angejardin-nodejs-staging.herokuapp.com"//"http://localhost:3000"//"https://angejardin-nodejs-staging.herokuapp.com"
  },
  backendSpringUsers: {
    baseURL: "https://angejardin-springusers-staging.herokuapp.com"//"http://localhost:9999"//"https://angejardin-springusers-staging.herokuapp.com"
  },
  backendSpringLegumes: {
    baseURL: "https://angejardin-sblegumes-staging.herokuapp.com"//"http://localhost:9998"//"https://angejardin-sblegumes-staging.herokuapp.com"
  },
  backendVille: {
    baseURL: "https://geo.api.gouv.fr"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
