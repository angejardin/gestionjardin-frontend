export const environment = {
  production: true,
  backendNodeJS: {
    baseURL: "https://angejardin-nodejs.herokuapp.com"
  },
  backendSpringUsers: {
    baseURL: "https://angejardin-springusers.herokuapp.com"
  },
  backendSpringLegumes: {
    baseURL: "https://angejardin-springlegumes.herokuapp.com"
  },
  backendVille: {
    baseURL: "https://geo.api.gouv.fr"
  }
};
